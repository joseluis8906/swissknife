package set_test

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"strconv"
	"swissknife/contracts"
	"swissknife/mocks"
	"swissknife/set"
)

var _ = Describe("Set", func() {
	var ctl *gomock.Controller
	var arr1 []contracts.T
	var arr2 []contracts.T

	BeforeSuite(func() {
		ctl = gomock.NewController(GinkgoT())

		for i := 0; i < 100000; i++ {
			tmp :=  mocks.NewMockT(ctl)
			var name string
			if i % 2 == 0 {
				name = "mock-0"
			} else {
				name = "mock-" + strconv.Itoa(i)
			}
			tmp.EXPECT().ToStr().Return(name).AnyTimes()
			arr1 = append(arr1, tmp)
		}

		for i := 0; i < 100000; i++ {
			tmp :=  mocks.NewMockT(ctl)
			tmp.EXPECT().ToStr().Return("mock-" + strconv.Itoa(i)).AnyTimes()
			arr2 = append(arr2, tmp)
		}
	})

	AfterSuite(func() {
		ctl.Finish()
	})

	It("should initialize with New and not be nil", func() {
		s := set.New()
		Expect(s).Should(Not(BeNil()))
	})

	It("should initialize from T slice and filter duplicates", func() {
		s := set.From(arr1)
		Expect(s).Should(Not(BeNil()))
		Expect(s.Size).Should(Equal((len(arr1)/2) + 1))
	})

	It("should adds new T object", func() {
		s := set.New()
		for _, it := range arr2 {
			s.Add(it)
		}
		Expect(s.Size).Should(Equal(len(arr2)))
	})

	It("should clear items", func() {
		s := set.From(arr1)
		s.Clear()
		Expect(s.Size).Should(Equal(0))
	})

	It("should delete item", func() {
		s := set.New()
		for idx, it := range arr2  {
			s.Add(it)
			if idx % 2 == 1 {
				s.Delete(arr2[idx])
			}
		}
		Expect(s.Size).Should(Equal(len(arr2)/2))
	})

	It("should apply fn to all items", func() {
		s := set.New()

		for i := 0; i < 3; i++ {
			s.Add(arr2[i])
		}

		s.ForEach(func(it contracts.T) {
			Expect(it.ToStr()).Should(Equal(it.ToStr()))
		})
	})

	It("should returns true if T elm exists", func() {
		s := set.New()
		idx := 100
		s.Add(arr2[idx])
		Expect(s.Has(arr2[idx])).Should(BeTrue())
		Expect(s.Has(arr2[idx + 1])).Should(BeFalse())
	})

	It("should returns slice of T objs", func() {
		s := set.From(arr2)
		Expect(s.Values()).Should(Equal(arr2))
	})
})

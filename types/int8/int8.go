package skint8

import (
	"strconv"
	"swissknife/contracts"
)

type Int8 int8

func New() Int8{
	return 0
}

func From(value Int8) Int8 {
	ins := New()
	ins = value
	return ins
}

func (i Int8) ToStr() string {
	return strconv.Itoa(int(i))
}

func (i Int8) Eq(another contracts.Comparable) bool {
	return i == another.(Int8)
}

func (i Int8) LessThan(another contracts.Comparable) bool {
	return i < another.(Int8)
}

func (i Int8) GreaterThan(another contracts.Comparable) bool {
	return i > another.(Int8)
}

func (i Int8) Add(another Int8) Int8 {
	return i + another
}

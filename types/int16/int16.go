package skint16

import (
	"strconv"
	"swissknife/contracts"
)

type Int16 int16

func New() Int16{
	return 0
}

func From(value Int16) Int16 {
	ins := New()
	ins = value
	return ins
}

func (i Int16) ToStr() string {
	return strconv.Itoa(int(i))
}

func (i Int16) Eq(another contracts.Comparable) bool {
	return i == another.(Int16)
}

func (i Int16) LessThan(another contracts.Comparable) bool {
	return i < another.(Int16)
}

func (i Int16) GreaterThan(another contracts.Comparable) bool {
	return i > another.(Int16)
}


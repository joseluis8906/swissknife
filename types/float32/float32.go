package skfloat32

import (
	"fmt"
	"swissknife/contracts"
)

type Float32 float32

func New() Float32{
	return 0
}

func From(value Float32) Float32 {
	ins := New()
	ins = value
	return ins
}

func (i Float32) ToStr() string {
	return fmt.Sprintf("%.6f", i)
}

func (i Float32) Eq(another contracts.Comparable) bool {
	return i == another.(Float32)
}

func (i Float32) LessThan(another contracts.Comparable) bool {
	return i < another.(Float32)
}

func (i Float32) GreaterThan(another contracts.Comparable) bool {
	return i > another.(Float32)
}

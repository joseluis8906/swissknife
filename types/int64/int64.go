package skint64

import (
	"strconv"
	"swissknife/contracts"
)

type Int64 int64

func New() Int64{
	return 0
}

func From(value Int64) Int64 {
	ins := New()
	ins = value
	return ins
}

func (i Int64) ToStr() string {
	return strconv.Itoa(int(i))
}

func (i Int64) Eq(another contracts.Comparable) bool {
	return i == another.(Int64)
}

func (i Int64) LessThan(another contracts.Comparable) bool {
	return i < another.(Int64)
}

func (i Int64) GreaterThan(another contracts.Comparable) bool {
	return i > another.(Int64)
}

package skint32

import (
	"strconv"
	"swissknife/contracts"
)

type Int32 int32

func New() Int32{
	return 0
}

func From(value Int32) Int32 {
	ins := New()
	ins = value
	return ins
}

func (i Int32) ToStr() string {
	return strconv.Itoa(int(i))
}

func (i Int32) Eq(another contracts.Comparable) bool {
	return i == another.(Int32)
}

func (i Int32) LessThan(another contracts.Comparable) bool {
	return i < another.(Int32)
}

func (i Int32) GreaterThan(another contracts.Comparable) bool {
	return i > another.(Int32)
}

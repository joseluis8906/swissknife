package skstring_test

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	skstring "swissknife/types/string"
)

var _ = Describe("String", func() {
	var ctl *gomock.Controller

	BeforeSuite(func() {
		ctl = gomock.NewController(GinkgoT())
	})

	AfterSuite(func() {
		ctl.Finish()
	})

	It("should initialize obj", func() {
		o := skstring.New()
		Expect(o).Should(Not(BeNil()))
	})

	It("should initialize from int", func() {
		o := skstring.From("default")
		Expect(o).Should(Not(BeNil()))
	})

	It("should compare equals", func() {
		o := skstring.From("default")
		o2 := skstring.From("default")

		Expect(o.Eq(o2)).Should(BeTrue())
	})

	It("should compare less than", func() {
		o := skstring.From("default1")
		o2 := skstring.From("default2")

		Expect(o.LessThan(o2)).Should(BeTrue())
	})

	It("should compare grater than", func() {
		o := skstring.From("default2")
		o2 := skstring.From("default1")

		Expect(o.GreaterThan(o2)).Should(BeTrue())
	})

	It("should represents to string", func() {
		o := skstring.From("default")
		str := "default"
		Expect(o.ToStr()).Should(Equal(str))
	})
})

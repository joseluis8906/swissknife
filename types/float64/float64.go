package skfloat64

import (
	"fmt"
	"swissknife/contracts"
)

type Float64 float64

func New() Float64{
	return 0
}

func From(value Float64) Float64 {
	ins := New()
	ins = value
	return ins
}

func (i Float64) ToStr() string {
	return fmt.Sprintf("%.6f", i)
}

func (i Float64) Eq(another contracts.Comparable) bool {
	return i == another.(Float64)
}

func (i Float64) LessThan(another contracts.Comparable) bool {
	return i < another.(Float64)
}

func (i Float64) GreaterThan(another contracts.Comparable) bool {
	return i > another.(Float64)
}

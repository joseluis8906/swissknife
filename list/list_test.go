package list_test

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"strconv"
	"strings"
	"swissknife/contracts"
	"swissknife/list"
	"swissknife/mocks"
	skint8 "swissknife/types/int8"
)

var _ = Describe("List", func() {
	var ctl *gomock.Controller
	var arr []contracts.T

	BeforeSuite(func() {
		ctl = gomock.NewController(GinkgoT())

		for i := 0; i < 100000; i++ {
			tmp :=  mocks.NewMockT(ctl)
			tmp.EXPECT().ToStr().Return("mock-" + strconv.Itoa(i)).AnyTimes()
			tmp.EXPECT().Eq(gomock.Any()).Return(true).AnyTimes()
			arr = append(arr, tmp)
		}
	})

	AfterSuite(func() {
		ctl.Finish()
	})

	It("should initialize obj", func() {
		l := list.New()
		Expect(l).Should(Not(BeNil()))
	})

	It("should initialize from", func() {
		l := list.From(arr)
		Expect(l.Length).Should(Equal(len(arr)))
	})

	It("should concat two list", func() {
		l1 := list.New()
		l2 := list.New()
		length := 3

		for i := 0; i < length; i++ {
			l1.Push(arr[i])
		}

		for j := 3; j < length + 3; j++ {
			l2.Push(arr[j])
		}

		l1.Concat(l2)
		Expect(l1.Length).Should(Equal(length * 2))
	})

	It("should apply test if a fn to each T elm in list is true", func() {
		l := list.New()

		for i := 0; i < 3; i++ {
			l.Push(arr[i])
		}

		res := l.Every(func(it contracts.T) bool {
			return it.Eq(it)
		})

		Expect(res).Should(BeTrue())
	})

	It("should filter all T obj that ends with 0", func() {
		l := list.From(arr)
		l2 := l.Filter(func(it contracts.T) bool {
			return strings.HasSuffix(it.ToStr(), "100")
		})

		Expect(l2.Length).Should(BeNumerically(">", 0))
	})

	It("should returns first match T elm", func() {
		l := list.From(arr)
		idx := 10
		el := l.Find(arr[idx])

		Expect(el).Should(Equal(arr[idx]))
	})

	It("should apply fn modifier to each elm in list", func() {
		l := list.New()

		for i := 0; i < 3; i++ {
			l.Push(arr[i])
		}

		l.ForEach(func(it contracts.T) {
			Expect(it.ToStr()).Should(Equal(it.ToStr()))
		})
	})

	It("should returns if elem exists", func() {
		l := list.From(arr)
		idx := 10
		elm := arr[idx]

		Expect(l.Includes(elm)).Should(BeTrue())
	})

	It("should join all items by prefix", func() {
		l := list.New()

		for i :=0; i < 3; i++ {
			l.Push(arr[i])
		}

		res := l.Join(",")
		Expect(strings.ContainsAny(res, ",")).Should(BeTrue())
	})

	It("should returns a list altered by fn", func() {
		l := list.From(arr)

		l2 := l.Map(func(t contracts.T) contracts.T {
			return skint8.From(1)
		})

		Expect(l2.Includes(skint8.From(1))).Should(BeTrue())
	})

	It("should returns a last T element", func() {
		l := list.New()

		for i :=0; i < 3; i++ {
			l.Push(arr[i])
		}

		Expect(l.Pop().ToStr()).Should(Equal(arr[2].ToStr()))
	})

	It("should append T element to end of list", func() {
		l := list.New()
		l.Push(arr[0])
		Expect(l.Length).Should(Equal(1))
	})

	It("should reduce the elements in a list to a scalar value", func() {
		l := list.New()

		for i := 0; i < 3; i++ {
			l.Push(skint8.From(skint8.Int8(i)))
		}

		rs := l.Reduce(func(acc contracts.T, cur contracts.T) contracts.T {
			return skint8.From(acc.(skint8.Int8).Add(cur.(skint8.Int8)))
		}, nil)

		Expect(rs).Should(Equal(skint8.Int8(3)))
	})

	It("should reverse the order of items", func() {
		l := list.New()

		for i := 0; i < 3; i++ {
			l.Push(skint8.From(skint8.Int8(i)))
		}

		l.Reverse()
		Expect(l.Pop()).Should(Equal(skint8.Int8(0)))
	})

	It("should sort items of a list", func() {
		l := list.New()

		for i := 2; i > 0; i-- {
			l.Push(skint8.From(skint8.Int8(i)))
		}

		l.Sort()
		Expect(l.Pop()).Should(Equal(skint8.Int8(2)))
	})
})

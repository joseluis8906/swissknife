module swissknife

go 1.13

require (
	github.com/golang/mock v1.4.0
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.9.0
)
